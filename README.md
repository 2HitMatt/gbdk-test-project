## Matts GBDK Test Project

This is a big mega project of various things I wanted to understand and test out with GBDK before some Game Boy related events kicked off. Its a mish mash of tutorials followed, code appropriated in and my own stuff.

## Resources I Learnt From
[GBDK 2020](https://github.com/gbdk-2020/gbdk-2020) - I'm using this version of the Game Boy Developers Kit.

[GamingMonsters GBDK Tutorials](https://www.youtube.com/watch?v=HIsWR_jLdwo&list=PLeEj4c2zF7PaFv5MPYhNAkBGrkx4iPGJo&index=1&ab_channel=GamingMonsters) - I'm mostly following these tutorials as a base

[Alphabet Tiles](https://videlais.com/2016/07/05/programming-game-boy-games-using-gbdk-part-3-using-gbtd-and-gbmb/) - I grabbed the alphabet tiles from here for my dialogue system. I added and removed some characters from it though.
 
[Print To Window](https://gbdev.gg8.se/forums/viewtopic.php?id=626) - Most of the text tutorials use printf, but that only outputs to background layer. I found this a good guide for rendering to the Window Layer.

[Check Collision](https://www.gamedev.net/tutorials/programming/general-and-gameplay-programming/swept-aabb-collision-detection-and-response-r3084/) - I used the AABB check from here. I was possibly going to add more of the physics from here, but I dont think I'll need to at this stage.

## Dialogue Output

![demo with dialogue](dialogue.gif)

This part is probably why you are looking at this project, so I figure I'll explain it here. As mentioned, the online tutorials mostly show text output with printf, but this outputs to the background layer which is annoying or problematic. In the coding guidelines it says to try to avoid printf and load font anyway?
**Font Tiles**
I have fontTiles that cover the basic numbers, letters, basic punctuation and a few smileys. At time of writing that's 50 tiles. Look at those .h and .c files for reference.

**TextWriter**
This does most of the work. So I'll break it into bullets.

 - I think background layer tiles are only 128 tiles? Anyway, I load the fontTiles in at the back of that. That way you can go nuts with your background tiles before those 50
 - I have a getChar method to get you the background tile index of a letter or keyboard character requested.
 - PrintToWin and PrintToWinDelayed glues the above together and takes a char* text array and gets a tile 1 character at a time and replaces whatever tile is at that position on the window layer. 
 - waitForInput is a helper that just stalls till a user presses something. Mostly for normal dialogue.
 
![demo with choice](choice.gif)
 
 - Here's where it gets interesting. In textWriter.h/c you can find 2
   methods to help with asking the user a 2 choice question: 
   printChoice takes a single char array but to make it work it needs to
   split it in 3 with \n characters. E.g "What you want?\n choice1\n
   
 - choice2"  getChoice lets the user bounce up and down between the 2   
   choices and when A is pressed, will return 0 or 1 depending which   
   option was picked (1 for first option as its usually the affirmative)

 

That its, look at the fontTiles.h/c and textWriter.h/c files and then in main.c to work with them. 

Cheers, 
[Matt Carr](https://twitter.com/2HitMatt)