;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.6 #12439 (MINGW32)
;--------------------------------------------------------
	.module Entity
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _currentMap
	.globl _setupBasicHitBox
	.globl _setSpriteIndexes
	.globl _setSpriteFrame
	.globl _applyPhysics
	.globl _updateColboxes
	.globl _updateSpritePosition
	.globl _checkCollision
	.globl _isMapPositionFree
	.globl _isOnGround
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_currentMap::
	.ds 8
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;Entity.c:5: void setupBasicHitBox(Entity* e){
;	---------------------------------
; Function setupBasicHitBox
; ---------------------------------
_setupBasicHitBox::
;Entity.c:6: e->colbox.w = 8;
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x000c
	add	hl, bc
	ld	(hl), #0x08
;Entity.c:7: e->colbox.h = 8;
	ld	hl, #0x000d
	add	hl, bc
	ld	(hl), #0x08
;Entity.c:8: e->colboxOffsetX = -4;
	ld	hl, #0x0008
	add	hl, bc
	ld	(hl), #0xfc
;Entity.c:9: e->colboxOffsetY = -4;
	ld	hl, #0x0009
	add	hl, bc
	ld	(hl), #0xfc
;Entity.c:10: updateColboxes(e);
	push	bc
	call	_updateColboxes
	pop	hl
;Entity.c:11: }
	ret
;Entity.c:13: void setSpriteIndexes(Entity* e, UINT8 spriteIndex){
;	---------------------------------
; Function setSpriteIndexes
; ---------------------------------
_setSpriteIndexes::
	dec	sp
;Entity.c:15: for(i = 0; i < 4; i++)
	ldhl	sp,	#3
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ldhl	sp,	#0
	ld	(hl), #0x00
00102$:
;Entity.c:17: e->spriteIndexes[i] = spriteIndex + i;
	ldhl	sp,	#0
	ld	l, (hl)
	ld	h, #0x00
	add	hl, bc
	ld	e, l
	ld	d, h
	ldhl	sp,	#5
	ld	a, (hl)
	ldhl	sp,	#0
	add	a, (hl)
	ld	(de), a
;Entity.c:15: for(i = 0; i < 4; i++)
	inc	(hl)
	ld	a, (hl)
	sub	a, #0x04
	jr	C, 00102$
;Entity.c:19: }
	inc	sp
	ret
;Entity.c:21: void setSpriteFrame(Entity* e, UINT8 tileIndex){
;	---------------------------------
; Function setSpriteFrame
; ---------------------------------
_setSpriteFrame::
	dec	sp
	dec	sp
;Entity.c:23: for(i = 0; i < 4; i++)
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ldhl	sp,	#1
	ld	(hl), #0x00
00103$:
;Entity.c:25: set_sprite_tile(e->spriteIndexes[i], tileIndex+i);
	ldhl	sp,	#6
	ld	a, (hl)
	ldhl	sp,	#1
	add	a, (hl)
	dec	hl
	ld	(hl+), a
	ld	l, (hl)
	ld	h, #0x00
	add	hl, bc
	ld	e, l
	ld	d, h
	ld	a, (de)
;c:/gbdk/include/gb/gb.h:1174: shadow_OAM[nb].tile=tile;
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	ld	de, #_shadow_OAM
	add	hl, de
	inc	hl
	inc	hl
	ld	e, l
	ld	d, h
	ldhl	sp,	#0
;Entity.c:23: for(i = 0; i < 4; i++)
	ld	a, (hl+)
	ld	(de), a
	inc	(hl)
	ld	a, (hl)
	sub	a, #0x04
	jr	C, 00103$
;Entity.c:27: }
	inc	sp
	inc	sp
	ret
;Entity.c:29: void applyPhysics(Entity* e){
;	---------------------------------
; Function applyPhysics
; ---------------------------------
_applyPhysics::
	add	sp, #-11
;Entity.c:31: UINT8 x = e->x + e->vx;
	ldhl	sp,	#13
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
	ldhl	sp,	#14
	ld	a, (hl)
	ldhl	sp,	#1
	ld	(hl), a
	pop	de
	push	de
	ld	hl, #0x0004
	add	hl, de
	push	hl
	ld	a, l
	ldhl	sp,	#4
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#3
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	pop	de
	push	de
	ld	hl, #0x0006
	add	hl, de
	push	hl
	ld	a, l
	ldhl	sp,	#6
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#5
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	a, (hl+)
	ld	d, a
	ld	a, (de)
	add	a, c
	ld	(hl), a
;Entity.c:32: UINT8 y = e->y + e->vy;  
	pop	de
	push	de
	ld	hl, #0x0005
	add	hl, de
	push	hl
	ld	a, l
	ldhl	sp,	#9
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#8
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	pop	de
	push	de
	ld	hl, #0x0007
	add	hl, de
	push	hl
	ld	a, l
	ldhl	sp,	#11
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#10
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, c
	ld	b, a
;Entity.c:34: if(isMapPositionFree(x,y,&currentMap) == 1)
	push	bc
	ld	de, #_currentMap
	push	de
	push	bc
	inc	sp
	ldhl	sp,	#11
	ld	a, (hl)
	push	af
	inc	sp
	call	_isMapPositionFree
	add	sp, #4
	pop	bc
	dec	e
	jr	NZ, 00102$
;Entity.c:36: e->x = x;
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ldhl	sp,	#6
;Entity.c:37: e->y = y;
	ld	a, (hl+)
	ld	(de), a
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), b
;Entity.c:38: updateColboxes(e);
	pop	de
	push	de
	push	de
	call	_updateColboxes
	pop	hl
	jr	00104$
00102$:
;Entity.c:44: e->vx = e->vx/2;
	ldhl	sp,	#4
	ld	a, (hl)
	ldhl	sp,	#7
	ld	(hl-), a
	dec	hl
	ld	a, (hl)
	ldhl	sp,	#8
	ld	(hl), a
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	a, (hl-)
	dec	hl
	ld	d, a
	ld	a, (de)
	ld	(hl+), a
	rlca
	sbc	a, a
	ld	(hl-), a
	ld	a, (hl+)
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	inc	hl
	ld	(hl-), a
	dec	hl
	bit	7, (hl)
	jr	Z, 00106$
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	l, e
	ld	h, d
	inc	hl
	push	hl
	ld	a, l
	ldhl	sp,	#7
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#6
	ld	(hl), a
00106$:
	ldhl	sp,#5
	ld	a, (hl+)
	ld	c, a
	ld	a, (hl+)
	ld	b, a
	sra	b
	rr	c
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;Entity.c:45: e->vy = e->vy/2;
	ldhl	sp,	#9
	ld	a, (hl-)
	dec	hl
	ld	(hl), a
	ldhl	sp,	#10
	ld	a, (hl-)
	dec	hl
	ld	(hl+), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	c, a
	rlca
	sbc	a, a
	ld	b, a
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, b
;	spillPairReg hl
;	spillPairReg hl
	bit	7, b
	jr	Z, 00107$
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, b
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
00107$:
	ld	c, l
	sra	h
	rr	c
	ldhl	sp,	#7
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	ld	(hl), c
;Entity.c:48: applyPhysics(e);
	pop	de
	push	de
	push	de
	call	_applyPhysics
	pop	hl
00104$:
;Entity.c:50: }
	add	sp, #11
	ret
;Entity.c:51: void updateColboxes(Entity* e){
;	---------------------------------
; Function updateColboxes
; ---------------------------------
_updateColboxes::
	dec	sp
;Entity.c:52: e->colbox.x= e->x+e->colboxOffsetX;
	ldhl	sp,	#3
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x000a
	add	hl, de
	ld	c, l
	ld	b, h
	ld	hl, #0x0004
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
	ld	hl, #0x0008
	add	hl, de
	ld	a, (hl)
	ldhl	sp,	#0
	ld	l, (hl)
;	spillPairReg hl
;	spillPairReg hl
	add	a, l
	ld	(bc), a
;Entity.c:53: e->colbox.y = e->y + e->colboxOffsetY;
	ld	hl, #0x000b
	add	hl, de
	ld	c, l
	ld	b, h
	ld	hl, #0x0005
	add	hl, de
	ld	l, (hl)
;	spillPairReg hl
	ld	a, e
	add	a, #0x09
	ld	e, a
	jr	NC, 00103$
	inc	d
00103$:
	ld	a, (de)
	add	a, l
	ld	(bc), a
;Entity.c:54: }
	inc	sp
	ret
;Entity.c:55: void updateSpritePosition(Entity* e){
;	---------------------------------
; Function updateSpritePosition
; ---------------------------------
_updateSpritePosition::
	add	sp, #-6
;Entity.c:57: move_sprite(e->spriteIndexes[0], e->x-4, e->y-4);
	ldhl	sp,	#8
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0005
	add	hl, bc
	inc	sp
	inc	sp
	ld	e, l
	ld	d, h
	push	de
	ld	a, (de)
	add	a, #0xfc
	ldhl	sp,	#4
	ld	(hl), a
	ld	hl, #0x0004
	add	hl, bc
	push	hl
	ld	a, l
	ldhl	sp,	#4
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#3
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	a, (hl+)
	inc	hl
	ld	d, a
	ld	a, (de)
	add	a, #0xfc
	ld	(hl), a
	ld	a, (bc)
;c:/gbdk/include/gb/gb.h:1247: OAM_item_t * itm = &shadow_OAM[nb];
	ld	l, a
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	ld	de, #_shadow_OAM
	add	hl, de
	ld	e, l
	ld	d, h
;c:/gbdk/include/gb/gb.h:1248: itm->y=y, itm->x=x;
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	(de), a
	inc	de
;Entity.c:59: move_sprite(e->spriteIndexes[1], e->x+4, e->y-4);
	ld	a, (hl-)
	ld	(de), a
	pop	de
	push	de
	ld	a, (de)
	add	a, #0xfc
	ld	(hl-), a
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	a, (hl+)
	inc	hl
	ld	d, a
	ld	a, (de)
	add	a, #0x04
	ld	(hl), a
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, b
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
;c:/gbdk/include/gb/gb.h:1247: OAM_item_t * itm = &shadow_OAM[nb];
	ld	l, (hl)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	ld	de, #_shadow_OAM
	add	hl, de
	ld	e, l
	ld	d, h
;c:/gbdk/include/gb/gb.h:1248: itm->y=y, itm->x=x;
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	(de), a
	inc	de
;Entity.c:61: move_sprite(e->spriteIndexes[2], e->x-4, e->y+4);
	ld	a, (hl-)
	ld	(de), a
	pop	de
	push	de
	ld	a, (de)
	add	a, #0x04
	ld	(hl-), a
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	a, (hl+)
	inc	hl
	ld	d, a
	ld	a, (de)
	add	a, #0xfc
	ld	(hl), a
	ld	l, c
;	spillPairReg hl
;	spillPairReg hl
	ld	h, b
;	spillPairReg hl
;	spillPairReg hl
	inc	hl
	inc	hl
;c:/gbdk/include/gb/gb.h:1247: OAM_item_t * itm = &shadow_OAM[nb];
	ld	l, (hl)
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	ld	de, #_shadow_OAM
	add	hl, de
	ld	e, l
	ld	d, h
;c:/gbdk/include/gb/gb.h:1248: itm->y=y, itm->x=x;
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	(de), a
	inc	de
	ld	a, (hl)
	ld	(de), a
;Entity.c:63: move_sprite(e->spriteIndexes[3], e->x+4, e->y+4);
	pop	de
	push	de
	ld	a, (de)
	add	a, #0x04
	ld	(hl-), a
	dec	hl
	dec	hl
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	add	a, #0x04
	ld	e, a
	inc	bc
	inc	bc
	inc	bc
	ld	a, (bc)
;c:/gbdk/include/gb/gb.h:1247: OAM_item_t * itm = &shadow_OAM[nb];
	ld	l, a
	ld	bc, #_shadow_OAM+0
;	spillPairReg hl
;	spillPairReg hl
	ld	h, #0x00
;	spillPairReg hl
;	spillPairReg hl
	add	hl, hl
	add	hl, hl
	add	hl, bc
	ld	c, l
	ld	b, h
;c:/gbdk/include/gb/gb.h:1248: itm->y=y, itm->x=x;
	ldhl	sp,	#5
	ld	a, (hl)
	ld	(bc), a
	inc	bc
	ld	a, e
	ld	(bc), a
;Entity.c:63: move_sprite(e->spriteIndexes[3], e->x+4, e->y+4);
;Entity.c:64: }
	add	sp, #6
	ret
;Entity.c:66: UBYTE checkCollision(Rect* a, Rect* b){
;	---------------------------------
; Function checkCollision
; ---------------------------------
_checkCollision::
	add	sp, #-10
;Entity.c:67: return !(a->x + b->w < b->x || a->x > b->x + b->w || a->y + a->h < b->y || a->y > b->y + b->h); 
	ldhl	sp,	#12
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
	ldhl	sp,	#13
	ld	a, (hl)
	ldhl	sp,	#1
	ld	(hl+), a
	pop	de
	push	de
	ld	a, (de)
	ld	(hl+), a
	ld	(hl), #0x00
	ldhl	sp,	#14
	ld	a, (hl)
	ldhl	sp,	#4
	ld	(hl), a
	ldhl	sp,	#15
	ld	a, (hl)
	ldhl	sp,	#5
	ld	(hl-), a
	ld	a, (hl+)
	ld	c, a
	ld	a, (hl+)
	ld	b, a
	inc	bc
	inc	bc
	ld	a, (bc)
	ld	(hl+), a
	ld	(hl), #0x00
	ldhl	sp,#2
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ldhl	sp,	#6
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ldhl	sp,	#8
	ld	(hl+), a
	xor	a, a
	ld	(hl-), a
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00132$
	bit	7, d
	jr	NZ, 00133$
	cp	a, a
	jr	00133$
00132$:
	bit	7, d
	jr	Z, 00133$
	scf
00133$:
	jp	C, 00104$
	ldhl	sp,#8
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ldhl	sp,	#6
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#2
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00134$
	bit	7, d
	jr	NZ, 00135$
	cp	a, a
	jr	00135$
00134$:
	bit	7, d
	jr	Z, 00135$
	scf
00135$:
	jp	C, 00104$
	pop	bc
	push	bc
	inc	bc
	ld	a, (bc)
	ldhl	sp,	#8
	ld	(hl+), a
	ld	(hl), #0x00
	pop	bc
	push	bc
	inc	bc
	inc	bc
	inc	bc
	ld	a, (bc)
	ld	d, #0x00
	ld	e, a
	ld	a, (hl-)
	ld	l, (hl)
	ld	h, a
	add	hl, de
	ld	c, l
	ld	b, h
	ldhl	sp,	#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	inc	de
	ld	a, (de)
	ldhl	sp,	#2
	ld	(hl+), a
	xor	a, a
	ld	(hl-), a
	ld	a, c
	sub	a, (hl)
	inc	hl
	ld	a, b
	sbc	a, (hl)
	ld	a, b
	ld	d, a
	bit	7, (hl)
	jr	Z, 00136$
	bit	7, d
	jr	NZ, 00137$
	cp	a, a
	jr	00137$
00136$:
	bit	7, d
	jr	Z, 00137$
	scf
00137$:
	jr	C, 00104$
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0003
	add	hl, de
	push	hl
	ld	a, l
	ldhl	sp,	#8
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#7
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	a, (de)
	ld	(hl), a
	ld	a, (hl-)
	ld	(hl+), a
	xor	a, a
	ld	(hl-), a
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ldhl	sp,	#2
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	push	hl
	ld	a, l
	ldhl	sp,	#6
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#5
	ld	(hl), a
	ldhl	sp,	#4
	ld	e, l
	ld	d, h
	ldhl	sp,	#8
	ld	a, (de)
	inc	de
	sub	a, (hl)
	inc	hl
	ld	a, (de)
	sbc	a, (hl)
	ld	a, (de)
	ld	d, a
	bit	7, (hl)
	jr	Z, 00138$
	bit	7, d
	jr	NZ, 00139$
	cp	a, a
	jr	00139$
00138$:
	bit	7, d
	jr	Z, 00139$
	scf
00139$:
	jr	C, 00104$
	ldhl	sp,	#9
	ld	(hl), #0x00
	jr	00105$
00104$:
	ldhl	sp,	#9
	ld	(hl), #0x01
00105$:
	ldhl	sp,	#9
	ld	a, (hl)
	xor	a, #0x01
	ld	e, a
;Entity.c:68: }
	add	sp, #10
	ret
;Entity.c:70: UBYTE isMapPositionFree(UINT8 x, UINT8 y, MapData* map ){
;	---------------------------------
; Function isMapPositionFree
; ---------------------------------
_isMapPositionFree::
	add	sp, #-10
;Entity.c:76: tileX = x / tileSize;
	ld	a, #0x08
	push	af
	inc	sp
	ldhl	sp,	#13
	ld	a, (hl)
	push	af
	inc	sp
	call	__divuchar
	pop	hl
	ldhl	sp,	#8
	ld	a, e
	ld	(hl+), a
	ld	(hl), #0x00
;Entity.c:77: tileY = (y-8) / tileSize;
	ldhl	sp,	#13
	ld	a, (hl)
	ld	b, #0x00
	add	a, #0xf8
	ld	c, a
	ld	a, b
	adc	a, #0xff
	ld	de, #0x0008
	push	de
	ld	b, a
	push	bc
	call	__divsint
	add	sp, #4
;Entity.c:79: tileIndex = map->w * tileY + tileX;
	ldhl	sp,	#14
	ld	a, (hl)
	ldhl	sp,	#0
	ld	(hl), a
	ldhl	sp,	#15
	ld	a, (hl)
	ldhl	sp,	#1
	ld	(hl-), a
	push	de
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	hl, #0x0005
	add	hl, de
	pop	de
	ld	c, l
	ld	b, h
	ld	a, (bc)
	ld	b, #0x00
	push	de
	ld	c, a
	push	bc
	call	__mulint
	add	sp, #4
	ldhl	sp,	#8
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	ld	c, l
	ld	a, h
	ldhl	sp,	#2
	ld	(hl), c
	inc	hl
;Entity.c:84: for(i = 0; i < map->numSolidTiles; i++){
	ld	(hl+), a
	pop	bc
	push	bc
	inc	bc
	inc	bc
	ld	e, c
	ld	d, b
	ld	a, (de)
	ld	(hl+), a
	inc	de
	ld	a, (de)
	ld	(hl), a
	pop	de
	push	de
	ld	hl, #0x0004
	add	hl, de
	push	hl
	ld	a, l
	ldhl	sp,	#8
	ld	(hl), a
	pop	hl
	ld	a, h
	ldhl	sp,	#7
	ld	(hl+), a
	xor	a, a
	ld	(hl+), a
	ld	(hl), a
00105$:
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	a, (hl+)
	ld	d, a
	ld	a, (de)
	ld	c, a
	ld	b, #0x00
	ld	a, (hl+)
	sub	a, c
	ld	a, (hl)
	sbc	a, b
	jr	NC, 00103$
;Entity.c:86: if(map->map[tileIndex] == map->solidTiles[i])
	pop	de
	push	de
	ld	a, (de)
	ld	c, a
	inc	de
	ld	a, (de)
	ld	b, a
	ldhl	sp,	#2
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, bc
	ld	c, l
	ld	b, h
	ld	a, (bc)
	ld	c, a
	ldhl	sp,#4
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ldhl	sp,	#8
	ld	a,	(hl+)
	ld	h, (hl)
	ld	l, a
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (de)
	ld	b, a
	ld	a, c
;Entity.c:89: return 0;//oh no, not safe
	sub	a,b
	jr	NZ, 00106$
	ld	e,a
	jr	00107$
00106$:
;Entity.c:84: for(i = 0; i < map->numSolidTiles; i++){
	ldhl	sp,	#8
	inc	(hl)
	jr	NZ, 00105$
	inc	hl
	inc	(hl)
	jr	00105$
00103$:
;Entity.c:94: return 1;
	ld	e, #0x01
00107$:
;Entity.c:96: }
	add	sp, #10
	ret
;Entity.c:97: UBYTE isOnGround(Entity* e){
;	---------------------------------
; Function isOnGround
; ---------------------------------
_isOnGround::
;Entity.c:98: if(isMapPositionFree(e->x, e->y+1, &currentMap) == 0)
	ldhl	sp,	#2
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	hl, #0x0005
	add	hl, bc
	ld	d, (hl)
	inc	d
	inc	bc
	inc	bc
	inc	bc
	inc	bc
	ld	a, (bc)
	ld	bc, #_currentMap
	push	bc
	push	de
	inc	sp
	push	af
	inc	sp
	call	_isMapPositionFree
	add	sp, #4
	ld	a, e
	or	a, a
;Entity.c:99: return 1;
;Entity.c:101: return 0;
	ld	e, #0x01
	ret	Z
	ld	e, #0x00
;Entity.c:102: }
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
