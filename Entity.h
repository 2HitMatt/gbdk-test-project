#define entityBank 0

#include <gb/gb.h>
#include <stdio.h>

typedef struct{
    const unsigned char *map;
    const unsigned char *solidTiles;
    UINT8 numSolidTiles;
    UINT8 w;
    UINT8 h;
    UINT8 bank;
}MapData;

extern MapData currentMap;

typedef struct{
    UINT8 x,y,w,h;
}Rect;

typedef struct {
    //UINT8 spriteIndex;
    UINT8 spriteIndexes[4];
    UINT8 x,y;
    INT8 vx, vy;
    INT8 colboxOffsetX, colboxOffsetY;
    Rect colbox;

} Entity;

void setupBasicHitBox(Entity* e);
void setSpriteIndexes(Entity* e, UINT8 spriteIndex);
void setSpriteFrame(Entity* e, UINT8 tileIndex);
void applyPhysics(Entity* e);
void updateColboxes(Entity* e);
void updateSpritePosition(Entity* e);
UBYTE checkCollision(Rect* a, Rect* b);
UBYTE isMapPositionFree(UINT8 x, UINT8 y, MapData* map );
UBYTE isOnGround(Entity* e);
