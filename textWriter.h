#ifndef TEXTWRITER
#define TEXTWRITER
#define textWriter1Bank 0

#include <gb/gb.h>
#include "fontTiles.h"
#include "blankScreen.h"
#include "gbt_player.h"//blarg, just so music doesnt stop when showing text :/

extern const UINT8 winWidth;
void betterDelay(UINT8 loops);
void betterDelayWithMusic(UINT8 loops);
void setFontTilesToBGData();
UINT8 getChar(char a);
void printToWin(UINT8 x, UINT8 y, char* text, UINT8 textLength);
void printToWinDelayed(UINT8 x, UINT8 y, char* text, UINT8 textLength);
void clearWin();
void waitForInput();
void printChoice(char* text, UINT8 textLength);
UBYTE  getChoice();


#endif