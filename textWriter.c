#include "textWriter.h"

const UINT8 winWidth = 19;

void betterDelay(UINT8 loops){
    UINT8 i;
    for(i = 0; i < loops; i++)
    {
        //wait until 1 screen drawn
        wait_vbl_done();
    }
}
void betterDelayWithMusic(UINT8 loops)
{
    UINT8 i;
    for(i = 0; i < loops; i++)
    {
        //wait until 1 screen drawn
        wait_vbl_done();
        gbt_update();
    }
}

void setFontTilesToBGData(){
    //it puts them in at the back, if the back is 128 tiles?
    set_bkg_data(fontTileIndex,numFontTiles,fontTiles);
}

UINT8 getChar(char a){
    if(a >= '0' && a <= '9')
    {
        return fontTileIndex + (a - '0');
    }
    if(a >= 'a' && a <= 'z'){
        return fontTileIndex + 17 + (a - 'a');
    }
    if(a >= 'A' && a <= 'Z'){
        return fontTileIndex + 17 + (a - 'A');
    }
    if(a == '>')
        return fontTileIndex + 14;
    if(a == '?')
        return fontTileIndex + 15;
    if(a == '!')
        return fontTileIndex + 46;
    if(a == '.')
        return fontTileIndex + 50;
    
    //TODO add more later


    return 0;
}

void printToWin(UINT8 x, UINT8 y, char* text, UINT8 textLength){
    //clearWin();
    UINT8 i;
    
    for(i = 0; i < textLength; i++){
        if(x > winWidth || text[i] == '\n')
        {
            y++;
            x = 0;
        }
        if(text[i] == '\n' || (x==0 && text[i]==' ')){
            //skip this output
        }
        else
        {
            UINT8 num = getChar(text[i]);
            set_win_tiles(x, y, 1, 1, &num);
            x++;
        }
        
    }
}

void printToWinDelayed(UINT8 x, UINT8 y, char* text, UINT8 textLength){
    clearWin();
    UINT8 i;
    for(i = 0; i < textLength; i++){
        if(x > winWidth || text[i] == '\n')
        {
            y++;
            x = 0;
        }
        if(text[i] == '\n' || (x==0 && text[i]==' ')){
            //skip this output
        }
        else
        {
            UINT8 num = getChar(text[i]);
            set_win_tiles(x, y, 1, 1, &num);
            x++;
        }
        betterDelayWithMusic(5);
    }
}

void clearWin(){
    set_win_tiles(0,0, 20, 18, blankScreen);
}

void waitForInput(){
    
    while(1){
        betterDelayWithMusic(1);
        UBYTE joypadState = joypad();
        if((joypadState & J_A) || (joypadState & J_B))
            break;
    }
}

//important! Assuming only 3 lines of text on dialogue box.
//so structure the string as "What you want?\n choice1\n choice2"
void printChoice(char* text, UINT8 textLength){
    clearWin();
    UINT8 i, x = 0, y =1;
    for(i = 0; i < textLength; i++){
        if(x > winWidth || text[i] == '\n')
        {
            y++;
            x = 0;
        }
        if(text[i] == '\n' || (x==0 && text[i]==' ')){
            //have choice 1 selected
            UINT8 num;
            if(y == 2){
                num = getChar('>');
            }
            else{
                num = getChar(' ');
            }
            set_win_tiles(x, y, 1, 1, &num);
            x++;

        }
        else
        {
            UINT8 num = getChar(text[i]);
            set_win_tiles(x, y, 1, 1, &num);
            x++;
        }
        betterDelayWithMusic(5);
    }
}

UBYTE  getChoice(){
    UBYTE currentChoice = 1;
    UINT8 selected = getChar('>');
    UINT8 notSelected = getChar(' ');
    while(1)
    {
        betterDelayWithMusic(1);
        UBYTE joypadState = joypad();
        if((joypadState & J_UP)&&currentChoice == 0)
        {
            currentChoice = 1;
            set_win_tiles(0, 2, 1, 1, &selected);
            set_win_tiles(0, 3, 1, 1, &notSelected);
        }
        if((joypadState & J_DOWN)&&currentChoice == 1)
        {
            currentChoice = 0;
            set_win_tiles(0, 2, 1, 1, &notSelected);
            set_win_tiles(0, 3, 1, 1, &selected);
        }
        if((joypadState & J_A))
        {
            break;
        }
    }
    return currentChoice;
}