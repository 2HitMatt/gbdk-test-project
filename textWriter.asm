;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.6 #12439 (MINGW32)
;--------------------------------------------------------
	.module textWriter
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _gbt_update
	.globl _set_win_tiles
	.globl _set_bkg_data
	.globl _wait_vbl_done
	.globl _joypad
	.globl _winWidth
	.globl _betterDelay
	.globl _betterDelayWithMusic
	.globl _setFontTilesToBGData
	.globl _getChar
	.globl _printToWin
	.globl _printToWinDelayed
	.globl _clearWin
	.globl _waitForInput
	.globl _printChoice
	.globl _getChoice
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;textWriter.c:5: void betterDelay(UINT8 loops){
;	---------------------------------
; Function betterDelay
; ---------------------------------
_betterDelay::
;textWriter.c:7: for(i = 0; i < loops; i++)
	ld	c, #0x00
00103$:
	ld	a, c
	ldhl	sp,	#2
	sub	a, (hl)
	ret	NC
;textWriter.c:10: wait_vbl_done();
	call	_wait_vbl_done
;textWriter.c:7: for(i = 0; i < loops; i++)
	inc	c
;textWriter.c:12: }
	jr	00103$
_winWidth:
	.db #0x13	; 19
;textWriter.c:13: void betterDelayWithMusic(UINT8 loops)
;	---------------------------------
; Function betterDelayWithMusic
; ---------------------------------
_betterDelayWithMusic::
;textWriter.c:16: for(i = 0; i < loops; i++)
	ld	c, #0x00
00103$:
	ld	a, c
	ldhl	sp,	#2
	sub	a, (hl)
	ret	NC
;textWriter.c:19: wait_vbl_done();
	call	_wait_vbl_done
;textWriter.c:20: gbt_update();
	push	bc
	call	_gbt_update
	pop	bc
;textWriter.c:16: for(i = 0; i < loops; i++)
	inc	c
;textWriter.c:22: }
	jr	00103$
;textWriter.c:24: void setFontTilesToBGData(){
;	---------------------------------
; Function setFontTilesToBGData
; ---------------------------------
_setFontTilesToBGData::
;textWriter.c:26: set_bkg_data(fontTileIndex,numFontTiles,fontTiles);
;setupPair	HL
	ld	a, (#_numFontTiles)
;setupPair	HL
	ld	hl, #_fontTileIndex
	ld	b, (hl)
	ld	de, #_fontTiles
	push	de
	push	af
	inc	sp
	push	bc
	inc	sp
	call	_set_bkg_data
	add	sp, #4
;textWriter.c:27: }
	ret
;textWriter.c:29: UINT8 getChar(char a){
;	---------------------------------
; Function getChar
; ---------------------------------
_getChar::
;textWriter.c:32: return fontTileIndex + (a - '0');
;setupPair	HL
	ld	hl, #_fontTileIndex
	ld	c, (hl)
;textWriter.c:30: if(a >= '0' && a <= '9')
	ldhl	sp,	#2
	ld	a, (hl)
	xor	a, #0x80
	sub	a, #0xb0
	jr	C, 00102$
	ld	e, (hl)
	ld	a,#0x39
	ld	d,a
	sub	a, (hl)
	bit	7, e
	jr	Z, 00170$
	bit	7, d
	jr	NZ, 00171$
	cp	a, a
	jr	00171$
00170$:
	bit	7, d
	jr	Z, 00171$
	scf
00171$:
	jr	C, 00102$
;textWriter.c:32: return fontTileIndex + (a - '0');
	ldhl	sp,	#2
	ld	a, (hl)
	add	a, #0xd0
	add	a, c
	ld	e, a
	ret
00102$:
;textWriter.c:35: return fontTileIndex + 17 + (a - 'a');
	ld	a, c
	add	a, #0x11
	ld	b, a
;textWriter.c:34: if(a >= 'a' && a <= 'z'){
	ldhl	sp,	#2
	ld	a, (hl)
	xor	a, #0x80
	sub	a, #0xe1
	jr	C, 00105$
	ld	e, (hl)
	ld	a,#0x7a
	ld	d,a
	sub	a, (hl)
	bit	7, e
	jr	Z, 00172$
	bit	7, d
	jr	NZ, 00173$
	cp	a, a
	jr	00173$
00172$:
	bit	7, d
	jr	Z, 00173$
	scf
00173$:
	jr	C, 00105$
;textWriter.c:35: return fontTileIndex + 17 + (a - 'a');
	ldhl	sp,	#2
	ld	a, (hl)
	add	a, #0x9f
	add	a, b
	ld	e, a
	ret
00105$:
;textWriter.c:37: if(a >= 'A' && a <= 'Z'){
	ldhl	sp,	#2
	ld	a, (hl)
	xor	a, #0x80
	sub	a, #0xc1
	jr	C, 00108$
	ld	e, (hl)
	ld	a,#0x5a
	ld	d,a
	sub	a, (hl)
	bit	7, e
	jr	Z, 00174$
	bit	7, d
	jr	NZ, 00175$
	cp	a, a
	jr	00175$
00174$:
	bit	7, d
	jr	Z, 00175$
	scf
00175$:
	jr	C, 00108$
;textWriter.c:38: return fontTileIndex + 17 + (a - 'A');
	ldhl	sp,	#2
	ld	a, (hl)
	add	a, #0xbf
	add	a, b
	ld	e, a
	ret
00108$:
;textWriter.c:40: if(a == '>')
	ldhl	sp,	#2
	ld	a, (hl)
	sub	a, #0x3e
	jr	NZ, 00111$
;textWriter.c:41: return fontTileIndex + 14;
	ld	a, c
	add	a, #0x0e
	ld	e, a
	ret
00111$:
;textWriter.c:42: if(a == '?')
	ldhl	sp,	#2
	ld	a, (hl)
	sub	a, #0x3f
	jr	NZ, 00113$
;textWriter.c:43: return fontTileIndex + 15;
	ld	a, c
	add	a, #0x0f
	ld	e, a
	ret
00113$:
;textWriter.c:44: if(a == '!')
	ldhl	sp,	#2
	ld	a, (hl)
	sub	a, #0x21
	jr	NZ, 00115$
;textWriter.c:45: return fontTileIndex + 46;
	ld	a, c
	add	a, #0x2e
	ld	e, a
	ret
00115$:
;textWriter.c:46: if(a == '.')
	ldhl	sp,	#2
	ld	a, (hl)
	sub	a, #0x2e
	jr	NZ, 00117$
;textWriter.c:47: return fontTileIndex + 50;
	ld	a, c
	add	a, #0x32
	ld	e, a
	ret
00117$:
;textWriter.c:52: return 0;
	ld	e, #0x00
;textWriter.c:53: }
	ret
;textWriter.c:55: void printToWin(UINT8 x, UINT8 y, char* text, UINT8 textLength){
;	---------------------------------
; Function printToWin
; ---------------------------------
_printToWin::
	dec	sp
	dec	sp
;textWriter.c:59: for(i = 0; i < textLength; i++){
	ldhl	sp,	#5
	ld	c, (hl)
	ld	b, #0x00
00111$:
	ld	a, b
	ldhl	sp,	#8
	sub	a, (hl)
	jr	NC, 00113$
;textWriter.c:60: if(x > winWidth || text[i] == '\n')
;setupPair	HL
	ld	a, (#_winWidth)
	ldhl	sp,	#1
	ld	(hl), a
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	l, b
	ld	h, #0x00
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (de)
	ld	d, a
	sub	a, #0x0a
	ld	a, #0x01
	jr	Z, 00142$
	xor	a, a
00142$:
	ld	e, a
	ldhl	sp,	#1
	ld	a, (hl)
	ldhl	sp,	#4
	sub	a, (hl)
	jr	C, 00101$
	ld	a, e
	or	a, a
	jr	Z, 00102$
00101$:
;textWriter.c:62: y++;
	inc	c
;textWriter.c:63: x = 0;
	ldhl	sp,	#4
	ld	(hl), #0x00
00102$:
;textWriter.c:65: if(text[i] == '\n' || (x==0 && text[i]==' ')){
	ld	a, e
	or	a, a
	jr	NZ, 00112$
	ldhl	sp,	#4
	ld	a, (hl)
	or	a, a
	jr	NZ, 00105$
	ld	a, d
	sub	a, #0x20
	jr	Z, 00112$
00105$:
;textWriter.c:70: UINT8 num = getChar(text[i]);
	push	bc
	push	de
	inc	sp
	call	_getChar
	inc	sp
	pop	bc
;textWriter.c:71: set_win_tiles(x, y, 1, 1, &num);
	ldhl	sp,#0
	ld	(hl), e
	push	hl
	ld	hl, #0x101
	push	hl
	ld	a, c
	push	af
	inc	sp
	ldhl	sp,	#9
	ld	a, (hl)
	push	af
	inc	sp
	call	_set_win_tiles
	add	sp, #6
;textWriter.c:72: x++;
	ldhl	sp,	#4
	inc	(hl)
00112$:
;textWriter.c:59: for(i = 0; i < textLength; i++){
	inc	b
	jr	00111$
00113$:
;textWriter.c:76: }
	inc	sp
	inc	sp
	ret
;textWriter.c:78: void printToWinDelayed(UINT8 x, UINT8 y, char* text, UINT8 textLength){
;	---------------------------------
; Function printToWinDelayed
; ---------------------------------
_printToWinDelayed::
	dec	sp
	dec	sp
;textWriter.c:79: clearWin();
	call	_clearWin
;textWriter.c:81: for(i = 0; i < textLength; i++){
	ldhl	sp,	#5
	ld	c, (hl)
	ld	b, #0x00
00111$:
	ld	a, b
	ldhl	sp,	#8
	sub	a, (hl)
	jr	NC, 00113$
;textWriter.c:82: if(x > winWidth || text[i] == '\n')
;setupPair	HL
	ld	a, (#_winWidth)
	ldhl	sp,	#1
	ld	(hl), a
	ldhl	sp,#6
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ld	l, b
	ld	h, #0x00
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (de)
	ld	d, a
	sub	a, #0x0a
	ld	a, #0x01
	jr	Z, 00142$
	xor	a, a
00142$:
	ld	e, a
	ldhl	sp,	#1
	ld	a, (hl)
	ldhl	sp,	#4
	sub	a, (hl)
	jr	C, 00101$
	ld	a, e
	or	a, a
	jr	Z, 00102$
00101$:
;textWriter.c:84: y++;
	inc	c
;textWriter.c:85: x = 0;
	ldhl	sp,	#4
	ld	(hl), #0x00
00102$:
;textWriter.c:87: if(text[i] == '\n' || (x==0 && text[i]==' ')){
	ld	a, e
	or	a, a
	jr	NZ, 00106$
	ldhl	sp,	#4
	ld	a, (hl)
	or	a, a
	jr	NZ, 00105$
	ld	a, d
	sub	a, #0x20
	jr	Z, 00106$
00105$:
;textWriter.c:92: UINT8 num = getChar(text[i]);
	push	bc
	push	de
	inc	sp
	call	_getChar
	inc	sp
	pop	bc
;textWriter.c:93: set_win_tiles(x, y, 1, 1, &num);
	ldhl	sp,#0
	ld	(hl), e
	push	hl
	ld	hl, #0x101
	push	hl
	ld	a, c
	push	af
	inc	sp
	ldhl	sp,	#9
	ld	a, (hl)
	push	af
	inc	sp
	call	_set_win_tiles
	add	sp, #6
;textWriter.c:94: x++;
	ldhl	sp,	#4
	inc	(hl)
00106$:
;textWriter.c:96: betterDelayWithMusic(5);
	push	bc
	ld	a, #0x05
	push	af
	inc	sp
	call	_betterDelayWithMusic
	inc	sp
	pop	bc
;textWriter.c:81: for(i = 0; i < textLength; i++){
	inc	b
	jr	00111$
00113$:
;textWriter.c:98: }
	inc	sp
	inc	sp
	ret
;textWriter.c:100: void clearWin(){
;	---------------------------------
; Function clearWin
; ---------------------------------
_clearWin::
;textWriter.c:101: set_win_tiles(0,0, 20, 18, blankScreen);
	ld	de, #_blankScreen
	push	de
	ld	hl, #0x1214
	push	hl
	xor	a, a
	rrca
	push	af
	call	_set_win_tiles
	add	sp, #6
;textWriter.c:102: }
	ret
;textWriter.c:104: void waitForInput(){
;	---------------------------------
; Function waitForInput
; ---------------------------------
_waitForInput::
;textWriter.c:106: while(1){
00105$:
;textWriter.c:107: betterDelayWithMusic(1);
	ld	a, #0x01
	push	af
	inc	sp
	call	_betterDelayWithMusic
	inc	sp
;textWriter.c:108: UBYTE joypadState = joypad();
	call	_joypad
;textWriter.c:109: if((joypadState & J_A) || (joypadState & J_B))
	bit	4, e
	ret	NZ
	bit	5, e
	jr	Z, 00105$
;textWriter.c:110: break;
;textWriter.c:112: }
	ret
;textWriter.c:116: void printChoice(char* text, UINT8 textLength){
;	---------------------------------
; Function printChoice
; ---------------------------------
_printChoice::
	add	sp, #-5
;textWriter.c:117: clearWin();
	call	_clearWin
;textWriter.c:118: UINT8 i, x = 0, y =1;
	ld	c, #0x00
;textWriter.c:119: for(i = 0; i < textLength; i++){
	ldhl	sp,	#3
	ld	a, #0x01
	ld	(hl+), a
	ld	(hl), #0x00
00114$:
	ldhl	sp,	#4
	ld	a, (hl)
	ldhl	sp,	#9
	sub	a, (hl)
	jp	NC, 00116$
;textWriter.c:120: if(x > winWidth || text[i] == '\n')
;setupPair	HL
	ld	hl, #_winWidth
	ld	b, (hl)
	ldhl	sp,#7
	ld	a, (hl+)
	ld	e, a
	ld	d, (hl)
	ldhl	sp,	#4
	ld	l, (hl)
	ld	h, #0x00
	add	hl, de
	ld	e, l
	ld	d, h
	ld	a, (de)
	ld	e, a
	sub	a, #0x0a
	ld	a, #0x01
	jr	Z, 00150$
	xor	a, a
00150$:
	ldhl	sp,	#1
	ld	(hl), a
	ld	a, b
	sub	a, c
	jr	C, 00101$
	ld	a, (hl)
	or	a, a
	jr	Z, 00102$
00101$:
;textWriter.c:122: y++;
	ldhl	sp,	#3
	inc	(hl)
;textWriter.c:123: x = 0;
	ld	c, #0x00
00102$:
;textWriter.c:135: x++;
	ld	a, c
	inc	a
	ldhl	sp,	#2
;textWriter.c:125: if(text[i] == '\n' || (x==0 && text[i]==' ')){
	ld	(hl-), a
	ld	a, (hl)
	or	a,a
	jr	NZ, 00107$
	or	a,c
	jr	NZ, 00108$
	ld	a, e
	sub	a, #0x20
	jr	NZ, 00108$
00107$:
;textWriter.c:128: if(y == 2){
	ldhl	sp,	#3
	ld	a, (hl)
	sub	a, #0x02
	jr	NZ, 00105$
;textWriter.c:129: num = getChar('>');
	push	bc
	ld	a, #0x3e
	push	af
	inc	sp
	call	_getChar
	inc	sp
	pop	bc
	ldhl	sp,	#0
	ld	(hl), e
	jr	00106$
00105$:
;textWriter.c:132: num = getChar(' ');
	push	bc
	ld	a, #0x20
	push	af
	inc	sp
	call	_getChar
	inc	sp
	pop	bc
	ldhl	sp,	#0
	ld	(hl), e
00106$:
;textWriter.c:134: set_win_tiles(x, y, 1, 1, &num);
	ldhl	sp,	#0
	push	hl
	ld	hl, #0x101
	push	hl
	ldhl	sp,	#7
	ld	a, (hl)
	push	af
	inc	sp
	ld	a, c
	push	af
	inc	sp
	call	_set_win_tiles
	add	sp, #6
;textWriter.c:135: x++;
	ldhl	sp,	#2
	ld	c, (hl)
	jr	00109$
00108$:
;textWriter.c:140: UINT8 num = getChar(text[i]);
	push	bc
	ld	a, e
	push	af
	inc	sp
	call	_getChar
	inc	sp
	pop	bc
;textWriter.c:141: set_win_tiles(x, y, 1, 1, &num);
	ldhl	sp,#0
	ld	(hl), e
	push	hl
	ld	hl, #0x101
	push	hl
	ldhl	sp,	#7
	ld	a, (hl)
	push	af
	inc	sp
	ld	a, c
	push	af
	inc	sp
	call	_set_win_tiles
	add	sp, #6
;textWriter.c:142: x++;
	ldhl	sp,	#2
	ld	c, (hl)
00109$:
;textWriter.c:144: betterDelayWithMusic(5);
	push	bc
	ld	a, #0x05
	push	af
	inc	sp
	call	_betterDelayWithMusic
	inc	sp
	pop	bc
;textWriter.c:119: for(i = 0; i < textLength; i++){
	ldhl	sp,	#4
	inc	(hl)
	jp	00114$
00116$:
;textWriter.c:146: }
	add	sp, #5
	ret
;textWriter.c:148: UBYTE  getChoice(){
;	---------------------------------
; Function getChoice
; ---------------------------------
_getChoice::
	dec	sp
	dec	sp
;textWriter.c:149: UBYTE currentChoice = 1;
	ld	c, #0x01
;textWriter.c:150: UINT8 selected = getChar('>');
	push	bc
	ld	a, #0x3e
	push	af
	inc	sp
	call	_getChar
	inc	sp
	pop	bc
	ldhl	sp,	#0
	ld	(hl), e
;textWriter.c:151: UINT8 notSelected = getChar(' ');
	push	bc
	ld	a, #0x20
	push	af
	inc	sp
	call	_getChar
	inc	sp
	pop	bc
	ldhl	sp,	#1
	ld	(hl), e
;textWriter.c:152: while(1)
00110$:
;textWriter.c:154: betterDelayWithMusic(1);
	push	bc
	ld	a, #0x01
	push	af
	inc	sp
	call	_betterDelayWithMusic
	inc	sp
	pop	bc
;textWriter.c:155: UBYTE joypadState = joypad();
	call	_joypad
	ld	b, e
;textWriter.c:156: if((joypadState & J_UP)&&currentChoice == 0)
	bit	2, b
	jr	Z, 00102$
	ld	a, c
	or	a, a
	jr	NZ, 00102$
;textWriter.c:158: currentChoice = 1;
	ld	c, #0x01
;textWriter.c:159: set_win_tiles(0, 2, 1, 1, &selected);
	ldhl	sp,	#0
	push	hl
	ld	hl, #0x101
	push	hl
	ld	hl, #0x200
	push	hl
	call	_set_win_tiles
	add	sp, #6
;textWriter.c:160: set_win_tiles(0, 3, 1, 1, &notSelected);
	ldhl	sp,	#1
	push	hl
	ld	hl, #0x101
	push	hl
	ld	hl, #0x300
	push	hl
	call	_set_win_tiles
	add	sp, #6
00102$:
;textWriter.c:162: if((joypadState & J_DOWN)&&currentChoice == 1)
	bit	3, b
	jr	Z, 00105$
	ld	a, c
;textWriter.c:164: currentChoice = 0;
	dec	a
	jr	NZ, 00105$
	ld	c,a
;textWriter.c:165: set_win_tiles(0, 2, 1, 1, &notSelected);
	ldhl	sp,	#1
	push	hl
	ld	hl, #0x101
	push	hl
	ld	hl, #0x200
	push	hl
	call	_set_win_tiles
	add	sp, #6
;textWriter.c:166: set_win_tiles(0, 3, 1, 1, &selected);
	ldhl	sp,	#0
	push	hl
	ld	hl, #0x101
	push	hl
	ld	hl, #0x300
	push	hl
	call	_set_win_tiles
	add	sp, #6
00105$:
;textWriter.c:168: if((joypadState & J_A))
	bit	4, b
	jr	Z, 00110$
;textWriter.c:173: return currentChoice;
	ld	e, c
;textWriter.c:174: }
	inc	sp
	inc	sp
	ret
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
