#include "Entity.h"

MapData currentMap;

void setupBasicHitBox(Entity* e){
    e->colbox.w = 8;
    e->colbox.h = 8;
    e->colboxOffsetX = -4;
    e->colboxOffsetY = -4;
    updateColboxes(e);
}

void setSpriteIndexes(Entity* e, UINT8 spriteIndex){
    UINT8 i;
    for(i = 0; i < 4; i++)
    {
        e->spriteIndexes[i] = spriteIndex + i;
    }
}

void setSpriteFrame(Entity* e, UINT8 tileIndex){
    UINT8 i;
    for(i = 0; i < 4; i++)
    {
        set_sprite_tile(e->spriteIndexes[i], tileIndex+i);
    }
}

void applyPhysics(Entity* e){

    UINT8 x = e->x + e->vx;
    UINT8 y = e->y + e->vy;  
    //is this a free tile on the map
    if(isMapPositionFree(x,y,&currentMap) == 1)
    {
        e->x = x;
        e->y = y;
        updateColboxes(e);
        
    }
    else
    {
        //we'll slice the velocity and hope for getting closer next turn then
        e->vx = e->vx/2;
        e->vy = e->vy/2;
        //e->vx = 0;
        //e->vy = 0;
        applyPhysics(e);
    }
}
void updateColboxes(Entity* e){
     e->colbox.x= e->x+e->colboxOffsetX;
    e->colbox.y = e->y + e->colboxOffsetY;
}
void updateSpritePosition(Entity* e){
    //top left
    move_sprite(e->spriteIndexes[0], e->x-4, e->y-4);
    //top right
    move_sprite(e->spriteIndexes[1], e->x+4, e->y-4);
    //bottom left
    move_sprite(e->spriteIndexes[2], e->x-4, e->y+4);
    //bottom right
    move_sprite(e->spriteIndexes[3], e->x+4, e->y+4);
}

UBYTE checkCollision(Rect* a, Rect* b){
    return !(a->x + b->w < b->x || a->x > b->x + b->w || a->y + a->h < b->y || a->y > b->y + b->h); 
}

UBYTE isMapPositionFree(UINT8 x, UINT8 y, MapData* map ){
    //TODO check if off map!

    //work out what tile we checking
    UINT8 tileSize = 8;
    UINT16 tileX, tileY, tileIndex;
    tileX = x / tileSize;
    tileY = (y-8) / tileSize;
    //get tile position for map array
    tileIndex = map->w * tileY + tileX;
    //printf("%u %u %u %u", tileX, tileY, tileIndex, map->numSolidTiles);
    //printf("\n %u sol ", map->map[tileIndex]);
    UINT16 i;
    //is it one of the solid tiles?
    for(i = 0; i < map->numSolidTiles; i++){
        //printf("%u ", map->solidTiles[i]);
        if(map->map[tileIndex] == map->solidTiles[i])
        {
            //printf("NOOO");
            return 0;//oh no, not safe
        }
    }
    //otherwise assume free spot
    //printf("FREE");
    return 1;

}
UBYTE isOnGround(Entity* e){
    if(isMapPositionFree(e->x, e->y+1, &currentMap) == 0)
        return 1;
    else
        return 0;
}