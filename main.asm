;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 4.1.6 #12439 (MINGW32)
;--------------------------------------------------------
	.module main
	.optsdcc -mgbz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _jump
	.globl _FadeScreenWhiteToNormal
	.globl _FadeScreenToWhite
	.globl _FadeScreenToBlackToNormal
	.globl _FadeScreenToBlack
	.globl _playChannel4Sound
	.globl _playSound
	.globl _playJumpSound
	.globl _getChoice
	.globl _printChoice
	.globl _waitForInput
	.globl _clearWin
	.globl _printToWinDelayed
	.globl _printToWin
	.globl _getChar
	.globl _setFontTilesToBGData
	.globl _betterDelay
	.globl _gbt_update
	.globl _gbt_loop
	.globl _gbt_play
	.globl _isOnGround
	.globl _checkCollision
	.globl _updateSpritePosition
	.globl _applyPhysics
	.globl _setSpriteFrame
	.globl _setSpriteIndexes
	.globl _setupBasicHitBox
	.globl _font_set
	.globl _font_load
	.globl _font_init
	.globl _sprintf
	.globl _set_sprite_data
	.globl _set_win_tiles
	.globl _set_bkg_tiles
	.globl _set_bkg_data
	.globl _set_interrupts
	.globl _disable_interrupts
	.globl _enable_interrupts
	.globl _joypad
	.globl _gravity
	.globl _otherGuy
	.globl _player
	.globl _i
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_i::
	.ds 1
_player::
	.ds 14
_otherGuy::
	.ds 14
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
_gravity::
	.ds 1
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;main.c:20: void playJumpSound(){
;	---------------------------------
; Function playJumpSound
; ---------------------------------
_playJumpSound::
;main.c:28: NR10_REG = 0x16; 
	ld	a, #0x16
	ldh	(_NR10_REG + 0), a
;main.c:35: NR11_REG = 0x40;
	ld	a, #0x40
	ldh	(_NR11_REG + 0), a
;main.c:44: NR12_REG = 0x73;  
	ld	a, #0x73
	ldh	(_NR12_REG + 0), a
;main.c:49: NR13_REG = 0x00;   
	xor	a, a
	ldh	(_NR13_REG + 0), a
;main.c:58: NR14_REG = 0xC3;
	ld	a, #0xc3
	ldh	(_NR14_REG + 0), a
;main.c:59: }
	ret
;main.c:62: void playSound(UINT8 a, UINT8 b, UINT8 c, UINT8 d, UINT8 e){
;	---------------------------------
; Function playSound
; ---------------------------------
_playSound::
;main.c:70: NR10_REG = a; 
	ldhl	sp,	#2
;main.c:77: NR11_REG = b;
	ld	a, (hl+)
	ldh	(_NR10_REG + 0), a
;main.c:86: NR12_REG = c;  
	ld	a, (hl+)
	ldh	(_NR11_REG + 0), a
;main.c:91: NR13_REG = d;   
	ld	a, (hl+)
	ldh	(_NR12_REG + 0), a
;main.c:100: NR14_REG = e;
	ld	a, (hl+)
	ldh	(_NR13_REG + 0), a
	ld	a, (hl)
	ldh	(_NR14_REG + 0), a
;main.c:101: }
	ret
;main.c:103: void playChannel4Sound(){
;	---------------------------------
; Function playChannel4Sound
; ---------------------------------
_playChannel4Sound::
;main.c:106: NR41_REG = 0x1F;
	ld	a, #0x1f
	ldh	(_NR41_REG + 0), a
;main.c:113: NR42_REG = 0xF1;
	ld	a, #0xf1
	ldh	(_NR42_REG + 0), a
;main.c:122: NR43_REG = 0x30;
	ld	a, #0x30
	ldh	(_NR43_REG + 0), a
;main.c:129: NR44_REG = 0xC0;  
	ld	a, #0xc0
	ldh	(_NR44_REG + 0), a
;main.c:130: }
	ret
;main.c:132: void FadeScreenToBlack(){
;	---------------------------------
; Function FadeScreenToBlack
; ---------------------------------
_FadeScreenToBlack::
;main.c:133: for(i = 0; i < 4; i++)
;setupPair	HL
	ld	hl, #_i
	ld	(hl), #0x00
00113$:
;main.c:135: if(i == 0){
;setupPair	HL
	ld	a, (#_i)
	or	a, a
	jr	NZ, 00110$
;main.c:136: BGP_REG = 0xE4;
	ld	a, #0xe4
	ldh	(_BGP_REG + 0), a
	jr	00111$
00110$:
;main.c:138: else if(i == 1){
;setupPair	HL
	ld	a, (#_i)
	dec	a
	jr	NZ, 00107$
;main.c:139: BGP_REG = 0xF9;
	ld	a, #0xf9
	ldh	(_BGP_REG + 0), a
	jr	00111$
00107$:
;main.c:141: else if(i == 2){
;setupPair	HL
	ld	a, (#_i)
	sub	a, #0x02
	jr	NZ, 00104$
;main.c:142: BGP_REG = 0xFE;
	ld	a, #0xfe
	ldh	(_BGP_REG + 0), a
	jr	00111$
00104$:
;main.c:144: else if(i == 3){
;setupPair	HL
	ld	a, (#_i)
	sub	a, #0x03
	jr	NZ, 00111$
;main.c:145: BGP_REG = 0xFF;
	ld	a, #0xff
	ldh	(_BGP_REG + 0), a
00111$:
;main.c:147: betterDelay(30);
	ld	a, #0x1e
	push	af
	inc	sp
	call	_betterDelay
	inc	sp
;main.c:133: for(i = 0; i < 4; i++)
;setupPair	HL
	ld	hl, #_i
	inc	(hl)
;setupPair	HL
	ld	a, (hl)
	sub	a, #0x04
	jr	C, 00113$
;main.c:149: }
	ret
;main.c:150: void FadeScreenToBlackToNormal(){
;	---------------------------------
; Function FadeScreenToBlackToNormal
; ---------------------------------
_FadeScreenToBlackToNormal::
;main.c:151: for(i = 0; i < 3; i++)
;setupPair	HL
	ld	hl, #_i
	ld	(hl), #0x00
00110$:
;main.c:153: if(i == 0){
;setupPair	HL
	ld	a, (#_i)
	or	a, a
	jr	NZ, 00107$
;main.c:154: BGP_REG = 0xFE;
	ld	a, #0xfe
	ldh	(_BGP_REG + 0), a
	jr	00108$
00107$:
;main.c:156: else if(i == 1){
;setupPair	HL
	ld	a, (#_i)
	dec	a
	jr	NZ, 00104$
;main.c:157: BGP_REG = 0xF9;
	ld	a, #0xf9
	ldh	(_BGP_REG + 0), a
	jr	00108$
00104$:
;main.c:159: else if(i == 2){
;setupPair	HL
	ld	a, (#_i)
	sub	a, #0x02
	jr	NZ, 00108$
;main.c:160: BGP_REG = 0xE4;
	ld	a, #0xe4
	ldh	(_BGP_REG + 0), a
00108$:
;main.c:162: betterDelay(30);
	ld	a, #0x1e
	push	af
	inc	sp
	call	_betterDelay
	inc	sp
;main.c:151: for(i = 0; i < 3; i++)
;setupPair	HL
	ld	hl, #_i
	inc	(hl)
;setupPair	HL
	ld	a, (hl)
	sub	a, #0x03
	jr	C, 00110$
;main.c:164: }
	ret
;main.c:166: void FadeScreenToWhite(){
;	---------------------------------
; Function FadeScreenToWhite
; ---------------------------------
_FadeScreenToWhite::
;main.c:167: for(i = 0; i < 4; i++)
;setupPair	HL
	ld	hl, #_i
	ld	(hl), #0x00
00113$:
;main.c:169: if(i == 0){
;setupPair	HL
	ld	a, (#_i)
	or	a, a
	jr	NZ, 00110$
;main.c:170: BGP_REG = 0xE4;
	ld	a, #0xe4
	ldh	(_BGP_REG + 0), a
	jr	00111$
00110$:
;main.c:172: else if(i == 1){
;setupPair	HL
	ld	a, (#_i)
	dec	a
	jr	NZ, 00107$
;main.c:173: BGP_REG = 0x90;
	ld	a, #0x90
	ldh	(_BGP_REG + 0), a
	jr	00111$
00107$:
;main.c:175: else if(i == 2){
;setupPair	HL
	ld	a, (#_i)
	sub	a, #0x02
	jr	NZ, 00104$
;main.c:176: BGP_REG = 0x40;
	ld	a, #0x40
	ldh	(_BGP_REG + 0), a
	jr	00111$
00104$:
;main.c:178: else if(i == 3){
;setupPair	HL
	ld	a, (#_i)
;main.c:179: BGP_REG = 0x00;
	sub	a,#0x03
	jr	NZ, 00111$
	ldh	(_BGP_REG + 0), a
00111$:
;main.c:181: betterDelay(30);
	ld	a, #0x1e
	push	af
	inc	sp
	call	_betterDelay
	inc	sp
;main.c:167: for(i = 0; i < 4; i++)
;setupPair	HL
	ld	hl, #_i
	inc	(hl)
;setupPair	HL
	ld	a, (hl)
	sub	a, #0x04
	jr	C, 00113$
;main.c:183: }
	ret
;main.c:184: void FadeScreenWhiteToNormal(){
;	---------------------------------
; Function FadeScreenWhiteToNormal
; ---------------------------------
_FadeScreenWhiteToNormal::
;main.c:185: for(i = 0; i < 3; i++)
;setupPair	HL
	ld	hl, #_i
	ld	(hl), #0x00
00110$:
;main.c:187: if(i == 0){
;setupPair	HL
	ld	a, (#_i)
	or	a, a
	jr	NZ, 00107$
;main.c:188: BGP_REG = 0x40;
	ld	a, #0x40
	ldh	(_BGP_REG + 0), a
	jr	00108$
00107$:
;main.c:190: else if(i == 1){
;setupPair	HL
	ld	a, (#_i)
	dec	a
	jr	NZ, 00104$
;main.c:191: BGP_REG = 0x90;
	ld	a, #0x90
	ldh	(_BGP_REG + 0), a
	jr	00108$
00104$:
;main.c:193: else if(i == 2){
;setupPair	HL
	ld	a, (#_i)
	sub	a, #0x02
	jr	NZ, 00108$
;main.c:194: BGP_REG = 0xE4;
	ld	a, #0xe4
	ldh	(_BGP_REG + 0), a
00108$:
;main.c:196: betterDelay(30);
	ld	a, #0x1e
	push	af
	inc	sp
	call	_betterDelay
	inc	sp
;main.c:185: for(i = 0; i < 3; i++)
;setupPair	HL
	ld	hl, #_i
	inc	(hl)
;setupPair	HL
	ld	a, (hl)
	sub	a, #0x03
	jr	C, 00110$
;main.c:198: }
	ret
;main.c:214: void jump(){
;	---------------------------------
; Function jump
; ---------------------------------
_jump::
;main.c:215: playJumpSound();
	call	_playJumpSound
;main.c:216: player.y = player.y - 1;//pull them off ground so we dont lose the jump straight away
	ld	bc, #_player + 5
	ld	a, (bc)
	dec	a
	ld	(bc), a
;main.c:217: player.vy = -12;
	ld	hl, #(_player + 7)
	ld	(hl), #0xf4
;main.c:218: }
	ret
;main.c:224: void main(){
;	---------------------------------
; Function main
; ---------------------------------
_main::
	add	sp, #-15
;main.c:228: NR52_REG = 0x80; // is 1000 0000 in binary and turns on sound
	ld	a, #0x80
	ldh	(_NR52_REG + 0), a
;main.c:229: NR50_REG = 0x77; // sets the volume for both left and right channel just set to max 0x77
	ld	a, #0x77
	ldh	(_NR50_REG + 0), a
;main.c:230: NR51_REG = 0xFF; // is 1111 1111 in binary, select which chanels we want to use in this case all of them. One bit for the L one bit for the R of all four channels
	ld	a, #0xff
	ldh	(_NR51_REG + 0), a
;main.c:232: disable_interrupts();
	call	_disable_interrupts
;main.c:234: gbt_play(song1_Data, 2, 7);
	ld	hl, #0x702
	push	hl
	ld	de, #_song1_Data
	push	de
	call	_gbt_play
	add	sp, #4
;main.c:235: gbt_loop(1);//yes loop
	ld	a, #0x01
	push	af
	inc	sp
	call	_gbt_loop
	inc	sp
;main.c:236: set_interrupts(VBL_IFLAG);
	ld	a, #0x01
	push	af
	inc	sp
	call	_set_interrupts
	inc	sp
;main.c:237: enable_interrupts();
	call	_enable_interrupts
;main.c:241: font_init();
	call	_font_init
;main.c:243: min_font = font_load(font_min);//36 font tiles in min
	ld	de, #_font_min
	push	de
	call	_font_load
	pop	hl
;main.c:244: font_set(min_font);
	push	de
	call	_font_set
	pop	hl
;main.c:251: set_bkg_data(37,9,map1Tiles);
	ld	de, #_map1Tiles
	push	de
	ld	hl, #0x925
	push	hl
	call	_set_bkg_data
	add	sp, #4
;main.c:252: setFontTilesToBGData();
	call	_setFontTilesToBGData
;main.c:254: set_bkg_tiles(0,0, 40, 18, map1);
	ld	de, #_map1
	push	de
	ld	hl, #0x1228
	push	hl
	xor	a, a
	rrca
	push	af
	xor	a, a
	call	_set_bkg_tiles
	add	sp, #6
;main.c:259: currentMap.map = map1;
	ld	hl, #_currentMap
	ld	(hl), #<(_map1)
	inc	hl
	ld	(hl), #>(_map1)
;main.c:260: currentMap.w = map1Width;
	ld	hl, #(_currentMap + 5)
	ld	(hl), #0x28
;main.c:261: currentMap.h = map1Height;
	ld	hl, #(_currentMap + 6)
	ld	(hl), #0x12
;main.c:262: currentMap.bank = map1TilesBank;
	ld	hl, #(_currentMap + 7)
	ld	(hl), #0x00
;main.c:263: currentMap.solidTiles = solidTiles;
	ld	hl, #(_currentMap + 2)
	ld	a, #<(_solidTiles)
	ld	(hl+), a
	ld	(hl), #>(_solidTiles)
;main.c:264: currentMap.numSolidTiles = map1NumSolidTiles;
	ld	hl, #(_currentMap + 4)
	ld	(hl), #0x06
;main.c:266: SHOW_BKG;
	ldh	a, (_LCDC_REG + 0)
	or	a, #0x01
	ldh	(_LCDC_REG + 0), a
;main.c:267: DISPLAY_ON;
	ldh	a, (_LCDC_REG + 0)
	or	a, #0x80
	ldh	(_LCDC_REG + 0), a
;main.c:271: set_win_tiles(0,0,5,1,windowMap);
	ld	de, #_windowMap
	push	de
	ld	hl, #0x105
	push	hl
	xor	a, a
	rrca
	push	af
	call	_set_win_tiles
	add	sp, #6
;c:/gbdk/include/gb/gb.h:1044: WX_REG=x, WY_REG=y;
	ld	a, #0x07
	ldh	(_WX_REG + 0), a
	ld	a, #0x70
	ldh	(_WY_REG + 0), a
;main.c:276: UINT8 num = getChar('G');
	ld	a, #0x47
	push	af
	inc	sp
	call	_getChar
	inc	sp
;main.c:283: FadeScreenToBlack();
	call	_FadeScreenToBlack
;main.c:284: FadeScreenToBlackToNormal();
	call	_FadeScreenToBlackToNormal
;main.c:285: betterDelay(100);
	ld	a, #0x64
	push	af
	inc	sp
	call	_betterDelay
	inc	sp
;main.c:286: FadeScreenToWhite();
	call	_FadeScreenToWhite
;main.c:287: FadeScreenWhiteToNormal();
	call	_FadeScreenWhiteToNormal
;main.c:289: SHOW_WIN;
	ldh	a, (_LCDC_REG + 0)
	or	a, #0x20
	ldh	(_LCDC_REG + 0), a
;main.c:291: ENABLE_RAM_MBC1;
	ld	hl, #0x0000
	ld	(hl), #0x0a
;main.c:293: if(savestate_newGame == 7)
;setupPair	HL
	ld	a, (#_savestate_newGame)
	sub	a, #0x07
	jr	NZ, 00105$
;main.c:295: printChoice("Continue?\nYes\nNew Game", 22);
	ld	a, #0x16
	push	af
	inc	sp
	ld	de, #___str_0
	push	de
	call	_printChoice
	add	sp, #3
;main.c:296: UINT8 c = getChoice();
	call	_getChoice
;main.c:298: if(c == 1)
	dec	e
	jr	Z, 00103$
;main.c:304: savestate_newGame = 0;
;setupPair	HL
	ld	hl, #_savestate_newGame
	ld	(hl), #0x00
00103$:
;main.c:306: clearWin();
	call	_clearWin
00105$:
;main.c:309: if(savestate_newGame != 7){
;setupPair	HL
	ld	a, (#_savestate_newGame)
	sub	a, #0x07
	jr	Z, 00110$
;main.c:311: printToWinDelayed(0,1,"Hello world!\n Lets fight to the death!", 37);
	ld	a, #0x25
	push	af
	inc	sp
	ld	de, #___str_1
	push	de
	xor	a, a
	inc	a
	push	af
	call	_printToWinDelayed
	add	sp, #5
;main.c:312: waitForInput();
	call	_waitForInput
;main.c:314: printChoice("How is this hat?\nSweet\nMeh..", 28);
	ld	a, #0x1c
	push	af
	inc	sp
	ld	de, #___str_2
	push	de
	call	_printChoice
	add	sp, #3
;main.c:315: UINT8 c = getChoice();
	call	_getChoice
;main.c:317: if(c == 1)
	dec	e
	jr	NZ, 00107$
;main.c:319: printToWinDelayed(0,1,"Cheers!", 7);
	ld	a, #0x07
	push	af
	inc	sp
	ld	de, #___str_3
	push	de
	ld	a, #0x01
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_printToWinDelayed
	add	sp, #5
	jr	00108$
00107$:
;main.c:323: printToWinDelayed(0,1,"You Suck!", 9);
	ld	a, #0x09
	push	af
	inc	sp
	ld	de, #___str_4
	push	de
	ld	a, #0x01
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_printToWinDelayed
	add	sp, #5
00108$:
;main.c:325: waitForInput();
	call	_waitForInput
;main.c:326: clearWin();
	call	_clearWin
;main.c:327: savestate_newGame = 7;//so we can skip dialogue next time
;setupPair	HL
	ld	hl, #_savestate_newGame
	ld	(hl), #0x07
;main.c:328: savestate_HP = 100;
;setupPair	HL
	ld	hl, #_savestate_HP
	ld	(hl), #0x64
00110$:
;main.c:330: DISABLE_RAM_MBC1;//disable saves basically
	ld	hl, #0x0000
	ld	(hl), #0x00
;main.c:337: UBYTE previousBank = _current_bank;
	ldh	a, (__current_bank + 0)
	ld	c, a
;main.c:339: SWITCH_ROM_MBC1(2);
	ld	a, #0x02
	ldh	(__current_bank + 0), a
	ld	h, #0x20
	ld	(hl), #0x02
;main.c:340: set_sprite_data(0,2,sprite1);
	ld	de, #_sprite1
	push	de
	ld	hl, #0x200
	push	hl
	call	_set_sprite_data
	add	sp, #4
;main.c:342: SWITCH_ROM_MBC1(previousBank);
	ld	a, c
	ldh	(__current_bank + 0), a
	ld	hl, #0x2000
	ld	(hl), c
;main.c:344: set_sprite_data(2, 8, metaSprites1);
	ld	de, #_metaSprites1
	push	de
	ld	hl, #0x802
	push	hl
	call	_set_sprite_data
	add	sp, #4
;c:/gbdk/include/gb/gb.h:1174: shadow_OAM[nb].tile=tile;
	ld	hl, #(_shadow_OAM + 2)
	ld	(hl), #0x00
;c:/gbdk/include/gb/gb.h:1247: OAM_item_t * itm = &shadow_OAM[nb];
	ld	hl, #_shadow_OAM
;c:/gbdk/include/gb/gb.h:1248: itm->y=y, itm->x=x;
	ld	a, #0x50
	ld	(hl+), a
	ld	(hl), #0x14
;main.c:352: SHOW_SPRITES;
	ldh	a, (_LCDC_REG + 0)
	or	a, #0x02
	ldh	(_LCDC_REG + 0), a
;main.c:362: setSpriteIndexes(&player, 1);
	ld	a, #0x01
	push	af
	inc	sp
	ld	de, #_player
	push	de
	call	_setSpriteIndexes
	add	sp, #3
;main.c:363: setSpriteFrame(&player, 2);
	ld	a, #0x02
	push	af
	inc	sp
	ld	de, #_player
	push	de
	call	_setSpriteFrame
	add	sp, #3
;main.c:364: player.x = 50;
	ld	hl, #(_player + 4)
	ld	(hl), #0x32
;main.c:365: player.y = 10;
	ld	hl, #(_player + 5)
	ld	(hl), #0x0a
;main.c:366: player.vx = 0;
	ld	hl, #(_player + 6)
	ld	(hl), #0x00
;main.c:367: player.vy = 0;
	ld	hl, #(_player + 7)
	ld	(hl), #0x00
;main.c:368: setupBasicHitBox(&player);
	ld	de, #_player
	push	de
	call	_setupBasicHitBox
	pop	hl
;main.c:369: updateSpritePosition(&player);
	ld	de, #_player
	push	de
	call	_updateSpritePosition
	pop	hl
;main.c:371: setSpriteIndexes(&otherGuy, 5);
	ld	a, #0x05
	push	af
	inc	sp
	ld	de, #_otherGuy
	push	de
	call	_setSpriteIndexes
	add	sp, #3
;main.c:372: setSpriteFrame(&otherGuy, 6);
	ld	a, #0x06
	push	af
	inc	sp
	ld	de, #_otherGuy
	push	de
	call	_setSpriteFrame
	add	sp, #3
;main.c:373: otherGuy.x = 50;
	ld	hl, #(_otherGuy + 4)
	ld	(hl), #0x32
;main.c:374: otherGuy.y = 50;
	ld	hl, #(_otherGuy + 5)
	ld	(hl), #0x32
;main.c:375: otherGuy.vx = 0;
	ld	hl, #(_otherGuy + 6)
	ld	(hl), #0x00
;main.c:376: otherGuy.vy = 0;
	ld	hl, #(_otherGuy + 7)
	ld	(hl), #0x00
;main.c:377: setupBasicHitBox(&otherGuy);
	ld	de, #_otherGuy
	push	de
	call	_setupBasicHitBox
	pop	hl
;main.c:378: updateSpritePosition(&otherGuy);
	ld	de, #_otherGuy
	push	de
	call	_updateSpritePosition
	pop	hl
;main.c:398: UINT8 currentFrame = 0;
	ldhl	sp,	#10
	ld	(hl), #0x00
;main.c:399: while(1){
	ldhl	sp,	#0
	ld	a, l
	ld	d, h
	ldhl	sp,	#11
	ld	(hl+), a
	ld	a, d
	ld	(hl-), a
	ld	a, (hl+)
	inc	hl
	ld	(hl-), a
	ld	a, (hl+)
	inc	hl
	ld	(hl), a
00132$:
;main.c:403: gbt_update();
	call	_gbt_update
;main.c:406: if(currentFrame == 0)
	ldhl	sp,	#10
	ld	a, (hl)
	or	a, a
	jr	NZ, 00112$
;main.c:408: currentFrame = 1;
	ld	(hl), #0x01
	jr	00113$
00112$:
;main.c:412: currentFrame = 0;
	ldhl	sp,	#10
	ld	(hl), #0x00
00113$:
;c:/gbdk/include/gb/gb.h:1174: shadow_OAM[nb].tile=tile;
	ld	de, #(_shadow_OAM + 2)
	ldhl	sp,	#10
	ld	a, (hl)
	ld	(de), a
;main.c:417: player.vx = 0;
	ld	hl, #(_player + 6)
	ld	(hl), #0x00
;main.c:419: UBYTE joypadState = joypad();
	call	_joypad
	ld	c, e
;main.c:420: if(joypadState & J_LEFT){
	bit	1, c
	jr	Z, 00115$
;main.c:422: player.vx = - 1;
	ld	hl, #(_player + 6)
	ld	(hl), #0xff
00115$:
;main.c:426: if(joypadState & J_RIGHT){
	bit	0, c
	jr	Z, 00117$
;main.c:428: player.vx =  1;
	ld	hl, #(_player + 6)
	ld	(hl), #0x01
00117$:
;main.c:438: if(joypadState & J_A){
	bit	4, c
	jr	Z, 00121$
;main.c:440: if(isOnGround(&player))
	push	bc
	ld	de, #_player
	push	de
	call	_isOnGround
	pop	hl
	ld	a, e
	pop	bc
	or	a, a
	jr	Z, 00121$
;main.c:442: jump();
	push	bc
	call	_jump
	pop	bc
00121$:
;main.c:445: if(joypadState & J_B){
	bit	5, c
	jr	Z, 00123$
;main.c:446: playChannel4Sound();
	call	_playChannel4Sound
;main.c:447: printToWinDelayed(0,1,"Howdy doody", 11);
	ld	a, #0x0b
	push	af
	inc	sp
	ld	de, #___str_5
	push	de
	xor	a, a
	inc	a
	push	af
	call	_printToWinDelayed
	add	sp, #5
00123$:
;main.c:452: if(isOnGround(&player) == 1)
	ld	de, #_player
	push	de
	call	_isOnGround
	pop	hl
	dec	e
	jr	NZ, 00125$
;main.c:455: player.vy = 0;
	ld	hl, #(_player + 7)
	ld	(hl), #0x00
	jr	00126$
00125$:
;main.c:460: player.vy = player.vy + 1;
	ld	a, (#(_player + 7) + 0)
	inc	a
	ld	(#(_player + 7)),a
00126$:
;main.c:463: if(checkCollision(&player.colbox, &otherGuy.colbox))
	ld	de, #(_otherGuy + 10)
	push	de
	ld	de, #(_player + 10)
	push	de
	call	_checkCollision
	add	sp, #4
	ld	a, e
	or	a, a
	jr	Z, 00130$
;main.c:465: ENABLE_RAM_MBC1;
	ld	hl, #0x0000
	ld	(hl), #0x0a
;main.c:466: savestate_HP -= 1;
;setupPair	HL
	ld	hl, #_savestate_HP
	dec	(hl)
;main.c:467: if(savestate_HP < 1)
;setupPair	HL
	ld	a, (hl)
	xor	a, #0x80
	sub	a, #0x81
	jr	NC, 00128$
;main.c:470: savestate_newGame = 0;
;setupPair	HL
	ld	hl, #_savestate_newGame
	ld	(hl), #0x00
;main.c:472: printToWinDelayed(0,1,"YOU DIED!", 9);
	ld	a, #0x09
	push	af
	inc	sp
	ld	de, #___str_6
	push	de
	xor	a, a
	inc	a
	push	af
	call	_printToWinDelayed
	add	sp, #5
;main.c:473: waitForInput();
	call	_waitForInput
;main.c:474: break;
	jr	00138$
00128$:
;main.c:480: sprintf(str, "%d", savestate_HP); 
;setupPair	HL
	ld	a, (#_savestate_HP)
	ld	e, a
	rlca
	sbc	a, a
	ldhl	sp,	#11
	ld	c, (hl)
	inc	hl
	ld	b, (hl)
	ld	d, a
	push	de
	ld	de, #___str_7
	push	de
	push	bc
	call	_sprintf
	add	sp, #6
;main.c:481: clearWin();
	call	_clearWin
;main.c:482: printToWin(0,0, str, 3);
	ldhl	sp,	#13
	ld	a, (hl+)
	ld	c, a
	ld	b, (hl)
	ld	a, #0x03
	push	af
	inc	sp
	push	bc
	xor	a, a
	rrca
	push	af
	call	_printToWin
	add	sp, #5
;main.c:483: DISABLE_RAM_MBC1;
	ld	hl, #0x0000
	ld	(hl), #0x00
;main.c:485: playChannel4Sound();
	call	_playChannel4Sound
00130$:
;main.c:489: applyPhysics(&player);
	ld	de, #_player
	push	de
	call	_applyPhysics
	pop	hl
;main.c:491: updateSpritePosition(&player);
	ld	de, #_player
	push	de
	call	_updateSpritePosition
	pop	hl
;main.c:492: updateSpritePosition(&otherGuy);
	ld	de, #_otherGuy
	push	de
	call	_updateSpritePosition
	pop	hl
;main.c:493: betterDelay(1);
	ld	a, #0x01
	push	af
	inc	sp
	call	_betterDelay
	inc	sp
	jp	00132$
00138$:
;main.c:497: }
	add	sp, #15
	ret
___str_0:
	.ascii "Continue?"
	.db 0x0a
	.ascii "Yes"
	.db 0x0a
	.ascii "New Game"
	.db 0x00
___str_1:
	.ascii "Hello world!"
	.db 0x0a
	.ascii " Lets fight to the death!"
	.db 0x00
___str_2:
	.ascii "How is this hat?"
	.db 0x0a
	.ascii "Sweet"
	.db 0x0a
	.ascii "Meh.."
	.db 0x00
___str_3:
	.ascii "Cheers!"
	.db 0x00
___str_4:
	.ascii "You Suck!"
	.db 0x00
___str_5:
	.ascii "Howdy doody"
	.db 0x00
___str_6:
	.ascii "YOU DIED!"
	.db 0x00
___str_7:
	.ascii "%d"
	.db 0x00
	.area _CODE
	.area _INITIALIZER
__xinit__gravity:
	.db #0x01	; 1
	.area _CABS (ABS)
