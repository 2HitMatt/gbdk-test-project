#include <gb/gb.h>
#include <stdio.h>
#include "sprite1.h"
#include "map1Tiles.h"
#include "map1.h"
#include <gb/font.h>
#include "windowMap.h"
#include "Entity.h"
#include "metaSprites1.h"
#include "fontTiles.h"
#include "textWriter.h"
#include "gbt_player.h"


UINT8 i;
//to compile and run, type this into terminal
//.\make.bat


void playJumpSound(){
    // see https://github.com/bwhitman/pushpin/blob/master/src/gbsound.txt
            // chanel 1 register 0, Frequency sweep settings
            // 7	Unused
            // 6-4	Sweep time(update rate) (if 0, sweeping is off)
            // 3	Sweep Direction (1: decrease, 0: increase)
            // 2-0	Sweep RtShift amount (if 0, sweeping is off)
            // 0001 0110 is 0x16, sweet time 1, sweep direction increase, shift ammount per step 110 (6 decimal)
            NR10_REG = 0x16; 

            // chanel 1 register 1: Wave pattern duty and sound length
            // Channels 1 2 and 4
            // 7-6	Wave pattern duty cycle 0-3 (12.5%, 25%, 50%, 75%), duty cycle is how long a quadrangular  wave is "on" vs "of" so 50% (2) is both equal.
            // 5-0 sound length (higher the number shorter the sound)
            // 01000000 is 0x40, duty cycle 1 (25%), wave length 0 (long)
            NR11_REG = 0x40;

            // chanel 1 register 2: Volume Envelope (Makes the volume get louder or quieter each "tick")
            // On Channels 1 2 and 4
            // 7-4	(Initial) Channel Volume
            // 3	Volume sweep direction (0: down; 1: up)
            // 2-0	Length of each step in sweep (if 0, sweeping is off)
            // NOTE: each step is n/64 seconds long, where n is 1-7	
            // 0111 0011 is 0x73, volume 7, sweep down, step length 3
            NR12_REG = 0x73;  

            // chanel 1 register 3: Frequency LSbs (Least Significant bits) and noise options
            // for Channels 1 2 and 3
            // 7-0	8 Least Significant bits of frequency (3 Most Significant Bits are set in register 4)
            NR13_REG = 0x00;   

            // chanel 1 register 4: Playback and frequency MSbs
            // Channels 1 2 3 and 4
            // 7	Initialize (trigger channel start, AKA channel INIT) (Write only)
            // 6	Consecutive select/length counter enable (Read/Write). When "0", regardless of the length of data on the NR11 register, sound can be produced consecutively.  When "1", sound is generated during the time period set by the length data contained in register NR11.  After the sound is ouput, the Sound 1 ON flag, at bit 0 of register NR52 is reset.
            // 5-3	Unused
            // 2-0	3 Most Significant bits of frequency
            // 1100 0011 is 0xC3, initialize, no consecutive, frequency = MSB + LSB = 011 0000 0000 = 0x300
            NR14_REG = 0xC3;
}

//play using channel 1 or 2
void playSound(UINT8 a, UINT8 b, UINT8 c, UINT8 d, UINT8 e){
    // see https://github.com/bwhitman/pushpin/blob/master/src/gbsound.txt
            // chanel 1 register 0, Frequency sweep settings
            // 7	Unused
            // 6-4	Sweep time(update rate) (if 0, sweeping is off)
            // 3	Sweep Direction (1: decrease, 0: increase)
            // 2-0	Sweep RtShift amount (if 0, sweeping is off)
            // 0001 0110 is 0x16, sweet time 1, sweep direction increase, shift ammount per step 110 (6 decimal)
            NR10_REG = a; 

            // chanel 1 register 1: Wave pattern duty and sound length
            // Channels 1 2 and 4
            // 7-6	Wave pattern duty cycle 0-3 (12.5%, 25%, 50%, 75%), duty cycle is how long a quadrangular  wave is "on" vs "of" so 50% (2) is both equal.
            // 5-0 sound length (higher the number shorter the sound)
            // 01000000 is 0x40, duty cycle 1 (25%), wave length 0 (long)
            NR11_REG = b;

            // chanel 1 register 2: Volume Envelope (Makes the volume get louder or quieter each "tick")
            // On Channels 1 2 and 4
            // 7-4	(Initial) Channel Volume
            // 3	Volume sweep direction (0: down; 1: up)
            // 2-0	Length of each step in sweep (if 0, sweeping is off)
            // NOTE: each step is n/64 seconds long, where n is 1-7	
            // 0111 0011 is 0x73, volume 7, sweep down, step length 3
            NR12_REG = c;  

            // chanel 1 register 3: Frequency LSbs (Least Significant bits) and noise options
            // for Channels 1 2 and 3
            // 7-0	8 Least Significant bits of frequency (3 Most Significant Bits are set in register 4)
            NR13_REG = d;   

            // chanel 1 register 4: Playback and frequency MSbs
            // Channels 1 2 3 and 4
            // 7	Initialize (trigger channel start, AKA channel INIT) (Write only)
            // 6	Consecutive select/length counter enable (Read/Write). When "0", regardless of the length of data on the NR11 register, sound can be produced consecutively.  When "1", sound is generated during the time period set by the length data contained in register NR11.  After the sound is ouput, the Sound 1 ON flag, at bit 0 of register NR52 is reset.
            // 5-3	Unused
            // 2-0	3 Most Significant bits of frequency
            // 1100 0011 is 0xC3, initialize, no consecutive, frequency = MSB + LSB = 011 0000 0000 = 0x300
            NR14_REG = e;
}

void playChannel4Sound(){
    // bit 5-0 Sound length
            // 0001 1111 is 0x1F the maximum length
            NR41_REG = 0x1F;

            // volume envelope
            // bit 7-4 - Initial Volume of envelope (0-0Fh) (0=No Sound)
            // bit 3 - Envelope Direction (0=Decrease, 1=Increase)
            // bit 2-0 - Number of envelope sweep (n: 0-7) (If zero, stop envelope operation.)
            // 1111 0001 is 0xF1, start at full volume, fade down, 1 envelope sweep  (decimal)
            NR42_REG = 0xF1;

            // bit 7-4 - Shift Clock Frequency (s)
            // bit 3   - Counter Step/Width (0=15 bits, 1=7 bits)
            // bit 2-0 - Dividing Ratio of Frequencies (r)
            // The amplitude is randomly switched between high and low at the given frequency. 
            // A higher frequency will make the noise to appear 'softer'. 
            // When Bit 3 is set, the output will become more regular, and some frequencies will sound more like Tone than Noise.
            // 0011 0000 is 0x30, shift clock frequency 3, step 0, dividing ratio 0
            NR43_REG = 0x30;

            // bit 7   - Initial (1=Restart Sound)
            // bit 6   - Controls if last forever or stops when NR41 ends
            // (1=Stop output when length in NR41 expires)
            // bit 5-0	Unused
            // 1100 0000, start sound, not continuous
            NR44_REG = 0xC0;  
}

void FadeScreenToBlack(){
    for(i = 0; i < 4; i++)
    {
        if(i == 0){
            BGP_REG = 0xE4;
        }
        else if(i == 1){
            BGP_REG = 0xF9;
        }
        else if(i == 2){
            BGP_REG = 0xFE;
        }
        else if(i == 3){
            BGP_REG = 0xFF;
        }
        betterDelay(30);
    }
}
void FadeScreenToBlackToNormal(){
    for(i = 0; i < 3; i++)
    {
        if(i == 0){
            BGP_REG = 0xFE;
        }
        else if(i == 1){
            BGP_REG = 0xF9;
        }
        else if(i == 2){
            BGP_REG = 0xE4;
        }
        betterDelay(30);
    }
}

void FadeScreenToWhite(){
    for(i = 0; i < 4; i++)
    {
        if(i == 0){
            BGP_REG = 0xE4;
        }
        else if(i == 1){
            BGP_REG = 0x90;
        }
        else if(i == 2){
            BGP_REG = 0x40;
        }
        else if(i == 3){
            BGP_REG = 0x00;
        }
        betterDelay(30);
    }
}
void FadeScreenWhiteToNormal(){
    for(i = 0; i < 3; i++)
    {
        if(i == 0){
            BGP_REG = 0x40;
        }
        else if(i == 1){
            BGP_REG = 0x90;
        }
        else if(i == 2){
            BGP_REG = 0xE4;
        }
        betterDelay(30);
    }
}

UINT8 gravity = 1;
Entity player;


extern UINT8 savestate_newGame;
extern INT8 savestate_HP;


//UINT8 groundY = 100;

Entity otherGuy;

extern MapData currentMap;

void jump(){
    playJumpSound();
    player.y = player.y - 1;//pull them off ground so we dont lose the jump straight away
    player.vy = -12;
}
//TODO screenshake, somehow
//TODO rand
//TODO music
extern const unsigned char * song1_Data[];

void main(){

    //TURN ON SOUND
    // these registers must be in this specific order!
    NR52_REG = 0x80; // is 1000 0000 in binary and turns on sound
    NR50_REG = 0x77; // sets the volume for both left and right channel just set to max 0x77
    NR51_REG = 0xFF; // is 1111 1111 in binary, select which chanels we want to use in this case all of them. One bit for the L one bit for the R of all four channels

    disable_interrupts();
    //play song from bank 2 at speed 7(?)
    gbt_play(song1_Data, 2, 7);
    gbt_loop(1);//yes loop
    set_interrupts(VBL_IFLAG);
    enable_interrupts();


    //fonts!
    font_init();
    font_t min_font;
    min_font = font_load(font_min);//36 font tiles in min
    font_set(min_font);


    //printf("HELLO WURLD");
    //40 sprites at a time, 10 overlapping!

    //set bg tile data
    set_bkg_data(37,9,map1Tiles);
    setFontTilesToBGData();
    //set bg layout
    set_bkg_tiles(0,0, 40, 18, map1);

    
    

    currentMap.map = map1;
    currentMap.w = map1Width;
    currentMap.h = map1Height;
    currentMap.bank = map1TilesBank;
    currentMap.solidTiles = solidTiles;
    currentMap.numSolidTiles = map1NumSolidTiles;

    SHOW_BKG;
    DISPLAY_ON;

    //x,y,w,h,tiles BUT window still takes up
    //a whole gameboy screen for some dumb reason
    set_win_tiles(0,0,5,1,windowMap);
    //move it out of the way so it doesn't cover the whole screen
    move_win(7,112);

    //get the number 7
    UINT8 num = getChar('G');
    //set a position on the window
    //set_win_tiles(19, 2, 1, 1, &num);//set_win_tiles(print_x, print_y, 1, 1, &c);
    
    //printf("%u",num);


    FadeScreenToBlack();
    FadeScreenToBlackToNormal();
    betterDelay(100);
    FadeScreenToWhite();
    FadeScreenWhiteToNormal();
    
    SHOW_WIN;
    //enable save bank
    ENABLE_RAM_MBC1;
    //is not a new game!
    if(savestate_newGame == 7)
    {
        printChoice("Continue?\nYes\nNew Game", 22);
        UINT8 c = getChoice();
        //clearWin();
        if(c == 1)
        {
            //nothing, whatever
        }
        else
        {
            savestate_newGame = 0;
        }
        clearWin();
    }

    if(savestate_newGame != 7){
        //clearWin();
        printToWinDelayed(0,1,"Hello world!\n Lets fight to the death!", 37);
        waitForInput();
        //clearWin();
        printChoice("How is this hat?\nSweet\nMeh..", 28);
        UINT8 c = getChoice();
        //clearWin();
        if(c == 1)
        {
            printToWinDelayed(0,1,"Cheers!", 7);
        }
        else
        {
            printToWinDelayed(0,1,"You Suck!", 9);
        }
        waitForInput();
        clearWin();
        savestate_newGame = 7;//so we can skip dialogue next time
        savestate_HP = 100;
    }
    DISABLE_RAM_MBC1;//disable saves basically

    //you can only load up 255 tiles(frames) at any onetime: https://gbdk-2020.github.io/gbdk-2020/docs/api/gb_8h.html#a47132bf7f387c2518c507a0b868cbf4c
    //           (first tile, how many tiles, sprite data)
    
    //BANK TEST!!!! sprite1 is in bank 2
    //lets store previous bank
    UBYTE previousBank = _current_bank;
    //jump to bank sprite 1 lives on
    SWITCH_ROM_MBC1(2);
    set_sprite_data(0,2,sprite1);
    //return to previous bank
    SWITCH_ROM_MBC1(previousBank);

    set_sprite_data(2, 8, metaSprites1);
    //set first sprite to use first sprite data
    set_sprite_tile(0,0);
    //player meta sprite
    
    //move sprite 0 to x y
    move_sprite(0, 20, 80);

    SHOW_SPRITES;

    /*set_sprite_tile(1,2);
    set_sprite_tile(2,3);
    set_sprite_tile(3,4);
    set_sprite_tile(4,5);
    player.spriteIndexes[0] = 1;
    player.spriteIndexes[1] = 2;
    player.spriteIndexes[2] = 3;
    player.spriteIndexes[3] = 4;*/
    setSpriteIndexes(&player, 1);
    setSpriteFrame(&player, 2);
    player.x = 50;
    player.y = 10;
    player.vx = 0;
    player.vy = 0;
    setupBasicHitBox(&player);
    updateSpritePosition(&player);

    setSpriteIndexes(&otherGuy, 5);
    setSpriteFrame(&otherGuy, 6);
    otherGuy.x = 50;
    otherGuy.y = 50;
    otherGuy.vx = 0;
    otherGuy.vy = 0;
    setupBasicHitBox(&otherGuy);
    updateSpritePosition(&otherGuy);


    
    /*printf("%i", currentMap.map[230]);
    
    if(!isMapPositionFree(player.x, player.y, &currentMap) )
        {
           // printf("hit ground");
        }
        else
        {
            //printf("not on ground");
        }
    delay(100000);
    delay(100000);*/

    //move_sprite(0, playerLocation.x, playerLocation.y);
    

    UINT8 currentFrame = 0;
    while(1){
        //scroll the bg
        //scroll_bkg(1,0);

        gbt_update();


        if(currentFrame == 0)
        {
            currentFrame = 1;
        }
        else
        {
            currentFrame = 0;
        }
        set_sprite_tile(0,currentFrame);
        

        player.vx = 0;
       
        UBYTE joypadState = joypad();
        if(joypadState & J_LEFT){
            //scroll_sprite(0, -10, 0);
            player.vx = - 1;
           // move_sprite(0, playerLocation.x, playerLocation.y);

        }
        if(joypadState & J_RIGHT){
            //scroll_sprite(0, 10, 0);
            player.vx =  1;
           // move_sprite(0, playerLocation.x, playerLocation.y);

        }
        if(joypadState & J_UP){
            //scroll_sprite(0, 0, -10);
        }
        if(joypadState & J_DOWN){
            //scroll_sprite(0, 0, 10);
        }
        if(joypadState & J_A){
            //playJumpSound();
            if(isOnGround(&player))
            {
                jump();
            }
        }
        if(joypadState & J_B){
            playChannel4Sound();
            printToWinDelayed(0,1,"Howdy doody", 11);
            //playSound(0x12,0x40,0x73,0x00,0xaa);
            //playSound(0x16,0x40,0x73,0x00,0xaa);
        }
        //deal with gravity
        if(isOnGround(&player) == 1)
        {
            //player.y = groundY;
            player.vy = 0;
        }
        else
        {
            //in air, add the gravs
            player.vy = player.vy + 1;
        }

        if(checkCollision(&player.colbox, &otherGuy.colbox))
        {
            ENABLE_RAM_MBC1;
            savestate_HP -= 1;
            if(savestate_HP < 1)
            {
                
                savestate_newGame = 0;
                
                printToWinDelayed(0,1,"YOU DIED!", 9);
                waitForInput();
                break;
            }
            

            //output hp
            char str[10];
            sprintf(str, "%d", savestate_HP); 
            clearWin();
            printToWin(0,0, str, 3);
            DISABLE_RAM_MBC1;
            //printf("OW!");
            playChannel4Sound();
        }
        

        applyPhysics(&player);
        //applyPhysics(&otherGuy);
        updateSpritePosition(&player);
        updateSpritePosition(&otherGuy);
        betterDelay(1);
    }
    

}

/*switch (joypadState)
        {
        case J_LEFT:
            scroll_sprite(0, -10, 0);
            break;
        
        case J_RIGHT:
            scroll_sprite(0, 10, 0);
            break;
        case J_UP:
            scroll_sprite(0, 0, -10);
            break;
        case J_DOWN:
            scroll_sprite(0, 0, 10);
            break;
        case J_A:
            playJumpSound();
            break;
        case J_B:
            playChannel4Sound();
            //playSound(0x12,0x40,0x73,0x00,0xaa);
            //playSound(0x16,0x40,0x73,0x00,0xaa);
            break;
        }*/